import java.util.Scanner;

public class Juego {
    protected Laberinto laberinto;
    private Player player;
    private String direccion;
    protected int[][] matriz;
    protected Scanner s;
    private boolean f;
    private boolean colision;

    public Juego() {
        laberinto = new Laberinto();
        player = new Player();
        s = new Scanner(System.in);
        f = true;
        matriz = new int[15][15];
    }

    public int[][] cleanMatriz(int[][] matriz) {
        for (int i = 0; i < laberinto.getMaze().length; i++) {
            for (int j = 0; j < laberinto.getMaze().length; j++) {
                matriz[i][j] = laberinto.getMaze()[i][j];
            }
        }
        return matriz;
    }

    public boolean colisiona(int y, int x) {
        if (laberinto.getMaze()[y][x] == 0) {
            // continua el camino
            System.out.println("estas en el camino");
            return false;
        } else if (laberinto.getMaze()[y][x] == 1) {
            // choco con muro
            System.out.println("estas en una pared");
            return true;
        } else if (laberinto.getMaze()[y][x] == 3) {
            System.out.println("estas en el comienzo");
            return false;
        }
        return true;

    }

    public void gamePlay() {
        int posicionX;
        int posicionY;
        int i = 0;
        int j = 0;
        while (f == true) {
            cleanMatriz(matriz);
            while (i <= laberinto.getMaze().length) {
                posicionX = i;
                while (j <= laberinto.getMaze().length) {
                    posicionY = j;
                    matriz[i][j] = player.getPlayerId();// ubica el jugador en el mapa
                    if (matriz[i][j] == 2) {
                        System.out.println("Felicidads! acabaste el laberinto");
                        f = false;
                    } else if (colisiona(i, j) == false) {
                        laberinto.print(matriz);
                        posicionX = i;
                        posicionY = j;
                        System.out.println("En que direccion deseas moverte ahora?");
                        direccion = s.nextLine();
                        switch (direccion) {
                            case "i": // arriba
                                if (colisiona(posicionX - 1, posicionY) == false && matriz[i][j] != 3) {
                                    System.out.println("arriba");
                                    i = i - 1;
                                    // j--;
                                } else {
                                    // j--;
                                    i = posicionX;
                                }
                                break;
                            case "m": // abajo
                                if (colisiona(posicionX + 1, posicionY) == false) {
                                    System.out.println("abajo");
                                    i = i + 1;
                                    // j = posicionY;
                                } else {
                                    // j = posicionY;
                                    i = posicionX;
                                }
                                break;
                            case "j": // izquierda
                                if (colisiona(posicionX, posicionY - 1) == false) {
                                    System.out.println("izquierda");
                                    j = j - 1;
                                    i = posicionX;
                                } else {
                                    j = posicionY;
                                    i = posicionX;
                                }
                                break;
                            case "l": // derecha
                                if (colisiona(posicionX, posicionY + 1) == false) {
                                    System.out.println("derecha");
                                    j++;
                                    i = posicionX;
                                } else {
                                    j = posicionY;
                                    i = posicionX;
                                }
                                break;
                            default:
                                i = posicionX;
                                j--;
                                break;

                        }

                    } else {
                        j++;
                    }
                    // laberinto.print(matriz);
                    Laberinto.ClearConsole();
                    cleanMatriz(matriz);
                }
                i++;
            }
        }
    }

}
