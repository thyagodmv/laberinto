public class Laberinto {
    // private int[][] maze;
    private int[][] maze = {
            { 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
            { 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1 },
            { 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1 },
            { 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1 },
            { 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1 },
            { 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 1 },
            { 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1 },
            { 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1 },
            { 1, 0, 0, 1, 1, 0, 1, 0, 0, 0, 1, 1, 0, 1, 1 },
            { 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1 },
            { 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1 },
            { 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1 },
            { 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 0, 1 },
            { 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 2 },
            { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
    };

    public void setMaze(int[][] maze) {
        this.maze = maze;
    }

    public int[][] getMaze() {
        return maze;
    }

    public Laberinto() {
    }

    public void print(int[][] maze) {
        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze.length; j++) {
                int x = maze[i][j];
                if (x == 1) {
                    System.out.print(" # ");
                } else if (x == 2 || x == 3) {
                    System.out.print(" ! ");
                } else if (x == 0) {
                    System.out.print(" . ");
                } else if (x == 5) {
                    System.out.print(" @ ");
                }
            }
            System.out.println(" ");

        }
    }

    // codigo tomado de:
    // https://www.delftstack.com/es/howto/java/java-clear-console/
    // Author: Rupam Yadav
    public static void ClearConsole() {
        try {
            ProcessBuilder pb = new ProcessBuilder("clear");
            Process startProcess = pb.inheritIO().start();

            startProcess.waitFor();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}