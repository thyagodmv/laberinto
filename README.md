# LABERINTO

Este repositorio es con el fin de comparir el codigo de un juego en consola sobre completar el laberinto

![captura-Laberinto](https://gitlab.com/thyagodmv/laberinto/-/blob/main/CapturaLaverinto.JPG "Laberinto")

## Instrucciones
para moverse en el laberinto puedes usar las letras :
-arriba=
`i`
-derecha:
```
j 
```
-abajo:
```
m
```
-izquierda:
```
l
```
y para salirte puedes usar:
- "cntrl + c"

## Licencia
Este proyecto esta bajo la licenia "GPL V.3".


## Dependencias
En este proyecto se ha usado como lenguaje de programacion java openjdk y neo vim como editor de texto usando librerias como.

-[ ] [javero](https://github.com/UnLocoProgramando/Javero)
-[ ] Java open jdk
-[ ] neovim

